<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/pages/user/review/', "RestaurantController@reviewRestaurant" )->name('pages.restaurants.review');
Route::get('/pages/restaurants/', "HomeController@listRestaurants" )->name('pages.restaurants');
Route::get('/pages/restaurants/new', "HomeController@NewRestaurant" )->name('pages.restaurants.new');
//Order

Route::get('/pending/{order_id}', 'OrderController@pending');
Route::get('/processing/{order_id}', 'OrderController@processing');
Route::get('/completed/{order_id}', 'OrderController@completed');
Route::get('/cancelled/{order_id}', 'OrderController@cancelled');

//Menu Status
Route::get('menu/destock/{order_id}', 'MenuController@destock');
Route::get('menu/restock/{order_id}', 'MenuController@restock');
Route::get('menu/delete/{order_id}', 'MenuController@delete');


Route::get('/pages/driver/submit', "HomeController@submitDriver" )->name('pages.driver.submit');

Route::get('/pages/restaurants/{restaurant}', "HomeController@restaurantProfile" )->name('pages.restaurants.profile');
Route::get('/pages/restaurants/{restaurant}/menu', "HomeController@viewRestaurantMenu" )->name('pages.restaurants.menu');

Route::get('/portal/dashboard', "PortalController@index" )->name('dashboard');
Route::get('/portal/menu/all', "PortalController@menuList" )->name('menu.list');
Route::patch('/menu/add','MenuController@addMenu')->name('menu.add');
Route::get('/cart/delete{temp_id}','MenuController@deleteCart')->name('cart.delete');


Route::get('/portal/ingredient/all', "PortalController@ingredientList" )->name('ingredients.list');
Route::patch('/ingredient/{restaurant}/add','MenuController@addIngredient')->name('ingredient.add');


Route::get('/portal/generate/qr', "PortalController@qrCodeView" )->name('qr.generate.view');
Route::get('/qr/{restaurant}/response', "RestaurantController@qrCodeResponse" )->name('qr.response');

Route::get('/portal/gallery', "PortalController@gallery" )->name('gallery');
Route::patch('/gallery/{restaurant}/add','RestaurantController@addToGallery')->name('gallery.add');

Route::get('/portal/orders', "PortalController@viewOrders" )->name('orders');
Route::get('/portal/completed/orders', "PortalController@viewCompletedOrders" )->name('orders.completed');


Route::get('/portal/customer/orders', "PortalController@viewCustomerOrders" )->name('customer.orders');
Route::get('/portal/customer/completed/orders', "PortalController@viewCompletedCustomerOrders" )->name('customer.completed.orders');



Route::get('/', 'HomeController@index')->name('landing');
Route::get('/search/','HomeController@searchItems')->name('item.search');

Route::get('/menu/{menu}/cart/add','OrderController@cartAdd')->name('cart.add');
Route::post('/restaurant/{restaurant}/cart/complete','OrderController@cartComplete')->name('cart.complete');

Route::get('/restaurant/{restaurant}/order/process', "OrderController@orderStep2")->name('order.step2');
Route::get('/restaurant/{restaurant}/order/finish', "OrderController@orderStep3")->name('order.step3');
Route::post('/order/status/update', "OrderController@updateOrderStatus")->name('order.update');



Auth::routes([
    'verify' =>true
]);

Route::get('/portal/profile', "PortalController@profile" )->name('profile');
Route::get('/portal/user/profile', "PortalController@UserProfile" )->name('user.profile');

Route::get('/restaurant/tables', "PortalController@viewTables" )->name('restaurant.tables');
// Route::get('/restaurant/profile', "PortalController@kulaPoints" )->name('kula.points');
Route::post('/restaurant/table/add', "PortalController@addTable" )->name('table.add');
Route::post('/restaurant/table/delete', "PortalController@deleteTable" )->name('table.delete');

Route::get('/restaurant/users', 'RestaurantController@showWaiters')->name('restaurant.users');

Route::post('/restaurant/waiter/add', "RestaurantController@addWaiter" )->name('waiter.add');

Route::patch('/portal/profile/update', "RestaurantController@updateProfile" )->name('profile.update');
Route::patch('/user/profile/update', "PortalController@updateProfile" )->name('user.update');
Route::get('/country/{country}/cities', 'SystemController@getCities')->name('country.cities');



Route::get('/restaurants/details', function () {
    return view('pages.details');
})->name('details');
Route::get('/faq', function () {
    return view('pages.faq');
})->name('faq');


Route::get('/order/cart2', function () {
    return view('pages.cart2');
})->name('cart2');
Route::get('/order/cart3', function () {
    return view('pages.cart3');
})->name('cart3');
Route::get('/diet/tips', function () {
    return view('pages.dtips');
})->name('d_tips');


Route::get('/about', function () {
    return view('pages.about');
})->name('about');
Route::get('/contact', function () {
    return view('pages.contact');
})->name('contact');
Route::get('/reservation', function () {
    return view('pages.reservation');
})->name('reservation');
Route::get('/tips', function () {
    return view('pages.tips');
})->name('tips');

Route::get('/cart', function () {
    return view('pages.cart');
})->name('cart');


Route::get('/wishes', function () {
    return view('pages.wishes');
})->name('wishes');


Route::get('/checkout', function () {
    return view('pages.checkout');
})->name('checkout');



Route::get('/admin/dashboard', 'AdminController@index')->name('admin.dashboard');
Route::get('/waiter/dashboard', 'RestaurantController@waiterDashboard')->name('waiter.dashboard');

Route::get('/admin/manage/users', 'AdminController@manageUsers')->name('admin.manage.users');


Route::get('/admin/restaurant/update', 'AdminController@UpdateStatus')->name('admin.restaurant.update');
Route::get('/suspend/{restaurant_profile_id}', 'AdminController@suspend');
Route::get('/activate/{restaurant_profile_id}', 'AdminController@activate');


Route::get('/admin/session/order', 'AdminController@keepOrderID')->name('admin.session.order');


Route::get('/menu', function () {
    return view('pages.menu');
})->name('menu');
Route::get('/menu/all',"HomeController@menuAll")->name('menu.all');
// Route::post('/menu/delete',"HomeController@delete")->name('menu.remove');

Route::get('/menu/{category?}',"HomeController@listRestaurants")->name('list-restaurants');
Route::get('/menu/restaurant/{id}',"HomeController@restaurantCategories")->name('list-category');


Route::get('/home', 'HomeController@index')->name('home');
Route::get('/verify', 'HomeController@verify')->name('verify');




/************ Mobile api Urls *******************/
#Route::post("/api/restaurants","MobileController@getAllRestaurants");
Route::post("/api/restaurant","MobileController@getRestaurants");
Route::post("/api/restaurant/menus","MobileController@getAllMenus");
Route::post("/api/restaurant/menu","MobileController@getMenu");
Route::post("/api/restaurant/tables","MobileController@getTables");

//Route::post("/api/restaurant/categories","MobileController@getMenuCategories");

Route::post('/api/user/login','MobileController@loginInUser');//e.g. api/user/auth?email=collins@gmail.com&password=collins
Route::post('/api/user/register','MobileController@registerUser');

Route::post("/api/categories/all","MobileController@getCategories");
Route::post("/api/menus/all","MobileController@getMenus");
//Route::post("/api/tips/all","MobileController@getTips");

Route::post("/api/user/reservations","MobileController@getReservations");
Route::post("/api/user/reservation/create","MobileController@newReservation");

Route::post("/api/user/orders","MobileController@getOrders");
Route::post("/api/order/create","MobileController@createOrder");
Route::post('/api/user/resetpassword', 'MobileController@resetPassword');
//role_id::1=admin 2=employer 3=candidate

Route::post("/api/restaurants/all","MobileController@getAllRestaurants");

/*
 *End of Mobile api routes
 */
